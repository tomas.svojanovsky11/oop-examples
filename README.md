## Základní principy

## Objekty a starší systémy

## Procedurální programování vs OOP
* Procedurální programovaní
* OOP

## Co je to objekt
* Data
* Behaviors

## Co je to třída
* Vytváření objektů
* attributy
* metody

## Zapouzdření a schování dat
* interface
* implementations

## Dědičnost
* superclass, subclass
* abstraction
* is-relationship

## Polymorphism

## Kompozice

## Závěr