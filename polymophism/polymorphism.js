class Shape {
    area = 0;

    constructor() {
        if (this.constructor === Shape) {
            throw new Error("This is abstract class");
        }
    }

    getArea() {
        throw new Error("Implement this");
    }
}

class Circle extends Shape {
    constructor(radius) {
        super();
        this.radius = radius;
    }

    getArea() {
       return 3.14 * (this.radius * this.radius);
    }
}

class Rectangle extends Shape {
    constructor(length, width) {
        super();
        this.length = length;
        this.width = width;
    }

    getArea() {
        return this.length * this.width;
    }
}
