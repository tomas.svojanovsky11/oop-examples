'use strict';

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/call

function Person(name, age, gender) {
    this.name = name;
    this.age = age;
    this.gender = gender;
}

Person.prototype.getName = function() {
    return this.name;
};

Person.prototype.getAge = function() {
    return this.age;
};

Person.prototype.getGender = function() {
    return this.gender;
};

function Teacher(name, age, gender, subject) {
    Person.call(this, name, age, gender);
    this.subject = subject;
}

Teacher.prototype = Object.create(Person.prototype);

Teacher.prototype.getSubject = function() {
    return this.subject;
};

function Student(name, age, gender, marks) {
    Person.call(this, name, age, gender);
    this.marks = marks;
}

Student.prototype = Object.create(Person.prototype);

Student.prototype.getMarks = function() {
    return this.marks;
};

const teacher = new Teacher('John Doe', 30, 'male', 'Maths');
const student = new Student('Jane Miles', 12, 'female', 88);

// class Person {
//     constructor(name, age, gender) {
//         this.name = name;
//         this.age = age;
//         this.gender = gender;
//     }
//
//     getName() {
//         return this.name;
//     }
//
//     getAge() {
//         return this.age;
//     }
//
//     getGender() {
//         return this.gender;
//     }
// }
//
// class Teacher extends Person {
//     constructor(name, age, gender, subject) {
//         super(name, age, gender);
//
//         this.subject = subject;
//     }
//
//     getSubject() {
//         return this.subject;
//     }
// }
//
// class Student extends Person {
//     constructor(name, age, gender, marks) {
//         super(name, age, gender);
//         this.marks = marks;
//     }
//
//     getMarks() {
//         return this.marks;
//     }
// }
//
// const teacher = new Teacher('John Doe', 30, 'male', 'Maths');
// const student = new Student('Jane Miles', 12, 'female', 88);
