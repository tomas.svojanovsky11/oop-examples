// The last challenge created an object with various properties. Now you'll see how to access the values of those properties. Here's an example:

var duck = {
    name: "Aflac",
    numLegs: 2
};
console.log(duck.name);
// Dot notation is used on the object name, duck, followed by the name of the property, name, to access the value of Aflac.