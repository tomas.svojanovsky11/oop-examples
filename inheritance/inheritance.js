class Mammal {
    constructor(eyeColor) {
        this.eyeColor = eyeColor;
    }

    getEyeColor() {
        return this.eyeColor;
    }
}

class Dog extends Mammal {
    interval;

    constructor(eyeColor, barkFrequency) {
        super(eyeColor);
        this.barkFrequency = barkFrequency;
    }

    bark() {
        this.interval = setInterval(() => {
            console.log("woof woof");
        }, this.barkFrequency);
    }

    stopBark() {
        clearInterval(this.interval);
    }
}

var doggy = new Dog("brown", 1000);
