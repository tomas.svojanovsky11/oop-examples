class SteeringWheel {
    steer() {
    }
}

class Engine {
    run() {
    }
}

class Car {
    constructor(steeringWheel, engine) {
        this.steeringWheel = steeringWheel;
        this.engine = engine;
    }

    steer() {
        this.steeringWheel.steer();
    }

    run() {
        this.engine.run();
    }
}

class Tractor {
    constructor(steeringWheel) {
        this.steeringWheel = steeringWheel;
    }

    steer() {
        this.steeringWheel.steer();
    }
}
