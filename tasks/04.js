// Modify the dog.sayLegs method to remove any references to dog.

let dog = {
    name: "Spot",
    numLegs: 4,
    sayLegs: function() {
        return "This dog has " + dog.numLegs + " legs.";
    },
};

dog.sayLegs();